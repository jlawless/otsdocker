Echo "operating system is "
cat /etc/os-release

###################################################
#Install spack
###################################################
export OTSDAQ_HOME=$PWD

cd ${OTSDAQ_HOME}
mkdir -p spack/repos
cd spack
git clone --single-branch https://github.com/FNALssi/spack.git -b fnal-develop
cd spack
git checkout $FNAL_DEVELOP_REF
cd ..
echo 'export SPACK_DISABLE_LOCAL_CONFIG=true
source spack/share/spack/setup-env.sh' > setup-env.sh
source setup-env.sh
git clone --single-branch https://github.com/fnalssi/fermi-spack-tools.git
cd fermi-spack-tools
git checkout $FERMI_SPACK_TOOLS
cd ..
./fermi-spack-tools/bin/make_packages_yaml spack

spack compiler find
spack env create ots
spack env activate ots
