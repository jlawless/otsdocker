echo "operating system is "
cat /etc/os-release



 
export KDIR=`ls -d -1 "/usr/src/kernels/"*/`

cd ..

cd /otsdaq
cd spack 
source setup-env.sh
cd ..

spack env activate ots
spack external find
spack compiler find 

###################################################
#Installing otsdaq suite
###################################################
cd /otsdaq
cd spack/repos


git clone --single-branch https://github.com/art-daq/artdaq-spack.git && spack repo add artdaq-spack
cd artdaq-spack
git checkout $ARTDAQ_SPACK_REF

cd /otsdaq
spack add otsdaq-suite@v2_08_00 artdaq=31207 s=126
spack concretize -f
spack install -j`nproc`

