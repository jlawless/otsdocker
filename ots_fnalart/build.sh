echo "operating system is "
cat /etc/os-release

cd /otsdaq
cd spack 
source setup-env.sh
cd ..

spack env activate ots

###################################################
#Installing art suite
###################################################
cd spack/repos
#git clone https://github.com/FNALssi/fnal_art.git && spack repo add fnal_art
git clone --single-branch -b eflumerf/DontUseMasterCMake https://github.com/eflumerf/fnal_art.git && spack repo add fnal_art
cd fnal_art
git checkout $FNAL_ART_REF


cd /otsdaq
spack add art-suite@s126
spack concretize -f
spack install -j`nproc`
