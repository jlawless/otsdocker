dnf install -y epel-release
dnf group install -y "Development Tools"
dnf install -y almalinux-release-devel

dnf install -y 'dnf-command(config-manager)'
dnf config-manager --add-repo  http://linuxsoft.cern.ch/cern/alma/9.3/CERN/x86_64/
dnf config-manager --set-enabled crb
dnf check-update 

rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-9
dnf -y install epel-release
dnf -y install $(cat $PWD/Packages)
